﻿CHARACTERS = {
	c:B19 ?= {
		create_character = { #Archprovost of Vincenton
			first_name = "Maurise"
			last_name = "Undercliffe"
			historical = yes
			ruler = yes
			age = 34
			interest_group = ig_devout
			ig_leader = yes
			ideology = ideology_abolitionist
			traits = {
				charismatic persistent
			}
		}
		create_character = {
			first_name = "Tristan"
			last_name = "Ritach"
			historical = yes
			age = 39
			interest_group = ig_intelligentsia
			ig_leader = yes
			ideology = ideology_mad_scientist
			traits = {
				scarred psychological_affliction
			}
		}
		create_character = {
			first_name = "Evin"
			last_name = Saiste
			historical = yes
			age = 47
			interest_group = ig_rural_folk
			ig_leader = yes
			ideology = ideology_slaver
			traits = {
				direct
			}
		}
	}
}
