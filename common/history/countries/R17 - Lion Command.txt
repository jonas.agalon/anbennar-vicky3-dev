﻿COUNTRIES = {
	c:R17 = {
		effect_starting_technology_tier_2_tech = yes
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_oligarchy #Until stratocracy is created
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_professional_army
		
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_dedicated_police #Militarized police not possible without mass surveillance
		activate_law = law_type:law_no_schools #Religious schools not possible with serfdom
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_women_own_property #Women in workplace not possible without feminism
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_legacy_slavery
		
		activate_law = law_type:law_all_races_allowed
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_pragmatic_artifice
		activate_law = law_type:law_artifice_only
	}
}