﻿POPS = {
	s:STATE_BRONRIN = {
		region_state:C02 = {
			create_pop = {
				culture = pearlsedger
				size = 342400
			}
			create_pop = {
				culture = costine
				size = 128400
			}
			create_pop = {
				culture = doos
				size = 205440
				split_religion = {
					doos = {
						ravelian = 0.8
						amadian_religion = 0.2
					}
				}
			}
			create_pop = {
				culture = green_orc
				size = 42800
			}
			create_pop = {
				culture = moon_elven
				size = 17120
			}
			create_pop = {
				culture = silver_dwarf
				size = 25680
			}
			create_pop = {
				culture = imperial_gnome
				size = 8560
			}
			create_pop = {
				culture = beefoot_halfling
				size = 85600
			}
		}
	}
	s:STATE_NUR_ISTRALORE = {
		region_state:C02 = {
			create_pop = {
				culture = east_damerian
				size = 111000
				religion = corinite
			}
			create_pop = {
				culture = west_damerian
				size = 14800
			}
			create_pop = {
				culture = green_orc
				size = 10360
			}
			create_pop = {
				culture = doos
				size = 5920
				split_religion = {
					doos = {
						ravelian = 0.4
						corinite = 0.4
						amadian_religion = 0.2
					}
				}
			}
			create_pop = {
				culture = moon_elven
				size = 1480
			}
			create_pop = {
				culture = beefoot_halfling
				size = 4440
			}
		}
	}
	s:STATE_MUNASIN = {
		region_state:C02 = {
			create_pop = {
				culture = pearlsedger
				size = 113400
			}
			create_pop = {
				culture = costine
				size = 45360
			}
			create_pop = {
				culture = green_orc
				size = 37800
			}
			create_pop = {
				culture = moon_elven
				size = 10080
			}
			create_pop = {
				culture = beefoot_halfling
				size = 7560
			}
			create_pop = {
				culture = doos
				size = 37800
				split_religion = {
					doos = {
						ravelian = 0.8
						amadian_religion = 0.2
					}
				}
			}
		}
	}
	s:STATE_SILVEGOR = {
		region_state:C02 = {
			create_pop = {
				culture = exwesser
				size = 103200
			}
			create_pop = {
				culture = doos
				size = 36120
				split_religion = {
					doos = {
						ravelian = 0.8
						amadian_religion = 0.2
					}
				}
			}
			create_pop = {
				culture = green_orc
				size = 20640
			}
			create_pop = {
				culture = moon_elven
				size = 12040
			}
		}
	}
	s:STATE_CLAMGUINN = {
		region_state:C02 = {
			create_pop = {
				culture = pearlsedger
				size = 66720
			}
			create_pop = {
				culture = doos
				size = 211280
				split_religion = {
					doos = {
						ravelian = 0.75
						amadian_religion = 0.25
					}
				}
			}
			create_pop = {
				culture = green_orc
				size = 13900
			}
		}
	}
	s:STATE_URANCESTIR = {
		region_state:C02 = {
			create_pop = {
				culture = pearlsedger
				size = 36400
			}
			create_pop = {
				culture = doos
				size = 114660
				split_religion = {
					doos = {
						ravelian = 0.75
						amadian_religion = 0.25
					}
				}
			}
			create_pop = {
				culture = green_orc
				size = 25480
			}
			create_pop = {
				culture = silver_dwarf
				size = 5460
			}
		}
	}
	s:STATE_CALILVAR = {
		region_state:C02 = {
			create_pop = {
				culture = vernman
				size = 12340
			}
			create_pop = {
				culture = doos
				size = 143210
				split_religion = {
					doos = {
						corinite = 0.8
						amadian_religion = 0.2
					}
				}
			}
		}
	}
	s:STATE_CYMBEAHN = {
		region_state:C02 = {
			create_pop = {
				culture = vernman
				size = 367920
			}
			create_pop = {
				culture = doos
				size = 74592
				split_religion = {
					doos = {
						corinite = 0.8
						amadian_religion = 0.2
					}
				}
			}
			create_pop = {
				culture = east_damerian
				size = 25200
			}
			create_pop = {
				culture = silver_dwarf
				size = 30240
			}
			create_pop = {
				culture = imperial_gnome
				size = 1010
			}
			create_pop = {
				culture = beefoot_halfling
				size = 5040
			}
		}
	}
	s:STATE_OOMU_NELIR = {
		region_state:C02 = {
			create_pop = {
				culture = vernman
				size = 21420
			}
			create_pop = {
				culture = pearlsedger
				size = 39780
			}
			create_pop = {
				culture = green_orc
				size = 39820
			}
			create_pop = {
				culture = doos
				size = 188190
				split_religion = {
					doos = {
						corinite = 0.75
						amadian_religion = 0.25
					}
				}
			}
		}
	}
	s:STATE_CARA_LAFQUEN = {
		region_state:C02 = {
			create_pop = {
				culture = vernman
				size = 52200
			}
			create_pop = {
				culture = doos
				size = 295800
				split_religion = {
					doos = {
						corinite = 0.75
						amadian_religion = 0.25
					}
				}
			}
		}
	}
	s:STATE_HARENAINE = {
		region_state:C11 = {
			create_pop = {
				culture = doos
				size = 107350
			}
			create_pop = {
				culture = vernman
				size = 5650
			}
		}
	}
	s:STATE_ARANLAS = {
		region_state:C02 = {
			create_pop = {
				culture = vernman
				size = 46000
			}
			create_pop = {
				culture = east_damerian
				size = 46400
			}
			create_pop = {
				culture = doos
				size = 21600
				split_religion = {
					doos = {
						corinite = 0.75
						amadian_religion = 0.25
					}
				}
			}
			create_pop = {
				culture = green_orc
				size = 12320
			}
			create_pop = {
				culture = silver_dwarf
				size = 4620
			}
			create_pop = {
				culture = imperial_gnome
				size = 1540
			}
		}
		region_state:C11 = {
			create_pop = {
				culture = doos
				size = 21400
			}
		}
	}
}