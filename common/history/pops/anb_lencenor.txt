﻿POPS = {
	s:STATE_ROILSARD = {
		region_state:A03 = {
			create_pop = {
				culture = roilsardi
				size = 4266221
			}
			create_pop = {
				culture = moon_elven
				size = 14323
				religion = corinite
			}
		}
	}

	s:STATE_ROSECROWN = {
		region_state:A03 = {
			create_pop = {
				culture = lorentish
				size = 2667044
			}
			create_pop = {
				culture = redfoot_halfling
				size = 777140
				split_religion = {
					redfoot_halfling = {
						regent_court = 0.7
						corinite = 0.3
					}
				}
			}
			create_pop = {
				culture = moon_elven
				size = 38785
				religion = regent_court
			}
			create_pop = {
				culture = ruby_dwarf
				size = 225404
			}
			create_pop = {
				culture = creek_gnome
				size = 9872
			}
		}
	}

	s:STATE_BLOODWINE = {
		region_state:A03 = {
			create_pop = {
				culture = lorentish
				size = 2804347
			}
			create_pop = {
				culture = redfoot_halfling
				size = 425740
				religion = regent_court
			}
		}
	}

	s:STATE_GREAT_ORDING = {
		region_state:A03 = {
			create_pop = {
				culture = lorentish
				size = 1547001
			}
		}
	}

	s:STATE_WINEBAY = {
		region_state:A03 = {
			create_pop = {
				culture = lorentish
				size = 1540561
				split_religion = {
					lorentish = {
						ravelian = 0.2	#growing
						regent_court = 0.8
					}
				}
			}
			create_pop = {
				culture = moon_elven
				size = 9139
				religion = regent_court
			}
			create_pop = {
				culture = redfoot_halfling
				size = 567145
				religion = regent_court
			}
			create_pop = {
				culture = ruby_dwarf
				size = 39270
			}
		}
	}

	s:STATE_SORNCOST = {
		region_state:A03 = {
			create_pop = {
				culture = sorncosti
				size = 1946638
			}
			create_pop = {
				culture = moon_elven
				size = 2856
				religion = regent_court
			}
		}
	}


	s:STATE_VENAIL = {
		region_state:A03 = {
			create_pop = {
				culture = sorncosti
				size = 426974
			}
			create_pop = {
				culture = moon_elven
				size = 8456
				religion = regent_court
			}
		}
	}

	s:STATE_RUBYHOLD = {
		region_state:A75 = {
			create_pop = {
				culture = ruby_dwarf
				size = 3723164
			}
		}
	}

	s:STATE_DAROM = {
		region_state:A03 = {
			create_pop = {
				culture = lorentish
				size = 1456322
			}
			create_pop = {
				culture = derannic
				size = 287217
			}
		}
	}

	s:STATE_DERANNE = {
		region_state:A03 = {
			create_pop = {
				culture = derannic
				size = 2945658
				split_religion = {
					derannic = {
						corinite = 0.2	#resistance. heavily presecuted
						regent_court = 0.8
					}
				}
			}
			create_pop = {
				culture = moon_elven	#Aelvar elves
				size = 8561
				religion = regent_court
			}
			create_pop = {
				culture = creek_gnome
				size = 26745
			}
		}
	}

	s:STATE_REDGLADES = {
		region_state:A65 = {
			create_pop = {
				culture = moon_elven
				size = 634201
				religion = regent_court
			}
		}
	}

	s:STATE_SOUTHROY = {
		region_state:A03 = {
			create_pop = {
				culture = lorentish
				size = 848561
			}
			create_pop = {
				culture = redfoot_halfling
				size = 642740
			}
		}
	}


	#Small Country
	s:STATE_ROYSFORT = {
		region_state:A14 = {
			create_pop = {
				culture = redfoot_halfling
				size = 1685573
			}
			create_pop = {
				culture = creek_gnome
				size = 8407
			}
		}
	}
	s:STATE_THOMSBRIDGE = {
		region_state:A14 = {
			create_pop = {
				culture = redfoot_halfling
				size = 1866460
				split_religion = {
					redfoot_halfling = {
						corinite = 0.25
						regent_court = 0.75
					}
				}
			}
		}
		region_state:A03 = {
			create_pop = {
				culture = lorentish
				size = 434561
			}
			create_pop = {
				culture = redfoot_halfling
				size = 702945
				religion = regent_court
			}
		}
	}
	s:STATE_CIDERFIELD = {
		region_state:A14 = {
			create_pop = {
				culture = bluefoot_halfling
				size = 1530257
				split_religion = {
					bluefoot_halfling = {
						corinite = 0.75
						regent_court = 0.25
					}
				}
			}
			create_pop = {
				culture = redfoot_halfling
				size = 1596287
				split_religion = {
					redfoot_halfling = {
						corinite = 0.75
						regent_court = 0.25
					}
				}
			}
			create_pop = {
				culture = creek_gnome
				size = 2941
			}
		}
	}
	s:STATE_HOMEHILLS = {
		region_state:A14 = {
			create_pop = {
				culture = redfoot_halfling
				size = 2522814
				split_religion = {
					redfoot_halfling = {
						regent_court = 0.5
						corinite = 0.5
					}
				}
			}
			create_pop = {
				culture = bluefoot_halfling
				size = 2021770
				split_religion = {
					bluefoot_halfling = {
						ravelian = 0.5
						corinite = 0.5
					}
				}
			}
			create_pop = {
				culture = creek_gnome
				size = 91028
			}
		}
	}
	s:STATE_ELKMARCH = {
		region_state:A14 = {
			create_pop = {
				culture = bluefoot_halfling
				size = 1684495
				split_religion = {
					bluefoot_halfling = {
						ravelian = 0.1
						corinite = 0.9
					}
				}
			}
		}
	}
	s:STATE_BEEPECK = {
		region_state:A14 = {
			create_pop = {
				culture = beefoot_halfling
				size = 1824495
			}
			create_pop = {
				culture = west_damerian
				size = 147002
			}
		}
	}
	s:STATE_DRAGONHILLS = {
		region_state:A14 = {
			create_pop = {
				culture = bluefoot_halfling
				size = 344442
				split_religion = {
					bluefoot_halfling = {
						ravelian = 0.1
						corinite = 0.9
					}
				}
			}
			create_pop = {
				culture = cliff_gnome
				size = 3869
			}
		}
	}
}