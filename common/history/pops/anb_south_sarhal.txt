﻿POPS = {

	s:STATE_DHAL_NIKHUVAD = {
		region_state:L05 = {
			create_pop = {
				culture = dhebiji
				size = 1118040
			}
			create_pop = {
				culture = adeeni_lizardfolk
				size = 56760
			}
			create_pop = {
				culture = merfolk
				size = 39600
			}
			create_pop = {
				culture = fangaulan
				size = 92400
			}
			create_pop = {
				culture = nalayni_harpy
				size = 13200
			}
		}
	}

	s:STATE_DHEBIJ_JANAB = {
		region_state:L05 = {
			create_pop = {
				culture = dhebiji
				size = 808412
			}
			create_pop = {
				culture = suhratbi
				size = 58020
			}
			create_pop = {
				culture = merfolk
				size = 13538
			}
			create_pop = {
				culture = fangaulan
				size = 8703
			}
		}
	}

	s:STATE_QASRIYINGI = {
		region_state:L05 = {
			create_pop = {
				culture = dhebiji
				size = 1059968
			}
			create_pop = {
				culture = adeeni_lizardfolk
				size = 101400
			}
			create_pop = {
				culture = merfolk
				size = 28392
			}
			create_pop = {
				culture = fangaulan
				size = 135200
			}
			create_pop = {
				culture = nalayni_harpy
				size = 27040
			}
		}
	}

	s:STATE_DEBIJ_SHAR = {
		region_state:L05 = {
			create_pop = {
				culture = dhebiji
				size = 514230
			}
			create_pop = {
				culture = adeeni_lizardfolk
				size = 50580
			}
			create_pop = {
				culture = fangaulan
				size = 30348
			}
		}
		region_state:L07 = {
			create_pop = {
				culture = ashamadi
				size = 107000
			}
			create_pop = {
				culture = fangaulan
				size = 40464
			}
		}
		region_state:L08 = {
			create_pop = {
				culture = ashamadi
				size = 70030
			}
			create_pop = {
				culture = fangaulan
				size = 30348
			}
		}
	}

	s:STATE_SUHRATBA_YAJ = {
		region_state:L05 = {
			create_pop = {
				culture = suhratbi
				size = 498960
			}
			create_pop = {
				culture = fangaulan
				size = 194040
			}
		}
	}

	s:STATE_BAASHI_BADDA = {
		region_state:L05 = {
			create_pop = {
				culture = suhratbi
				size = 1504125
			}
			create_pop = {
				culture = adeeni_lizardfolk
				size = 46413
			}
			create_pop = {
				culture = merfolk
				size = 56727
			}
			create_pop = {
				culture = fangaulan
				size = 85950
			}
			create_pop = {
				culture = tinker_gnome
				size = 8595
			}
			create_pop = {
				culture = soot_goblin
				size = 17190
			}
		}
	}

	s:STATE_SUHRATBA_YAHIN = {
		region_state:L05 = {
			create_pop = {
				culture = suhratbi
				size = 303780
			}
			create_pop = {
				culture = fangaulan
				size = 62220
			}
		}
	}

	s:STATE_DHAL_TANIZUUD = {
		region_state:L06 = {
			create_pop = {
				culture = ashamadi
				size = 749800
			}
			create_pop = {
				culture = merfolk
				size = 9200
			}
			create_pop = {
				culture = tanizu
				size = 18400
			}
			create_pop = {
				culture = fangaulan
				size = 128800
			}
			create_pop = {
				culture = nalayni_harpy
				size = 13800
			}
		}
	}

	s:STATE_JURITAQA = {
		region_state:L06 = {
			create_pop = {
				culture = ashamadi
				size = 1209600
			}
			create_pop = {
				culture = fangaulan
				size = 230400
			}
		}
	}

	s:STATE_MPAKA = {
		region_state:L06 = {
			create_pop = {
				culture = ashamadi
				size = 149600
			}
			create_pop = {
				culture = tanizu
				size = 704000
			}
			create_pop = {
				culture = fangaulan
				size = 26400
			}
		}
	}

	s:STATE_ASHAMAD_BARIGA = {
		region_state:L08 = {
			create_pop = {
				culture = ashamadi
				size = 1388400
			}
			create_pop = {
				culture = fangaulan
				size = 373800
			}
			create_pop = {
				culture = nalayni_harpy
				size = 17800
			}
		}
	}

	s:STATE_DHAI_BAEIDAG = {
		region_state:L08 = {
			create_pop = {
				culture = ashamadi
				size = 634740
			}
			create_pop = {
				culture = adeeni_lizardfolk
				size = 21300
			}
			create_pop = {
				culture = fangaulan
				size = 195960
			}
		}
	}

	s:STATE_QASRI_ABEESOOYINKA = {
		region_state:L07 = {
			create_pop = {
				culture = ashamadi
				size = 2601600
			}
			create_pop = {
				culture = adeeni_lizardfolk
				size = 64000
			}
			create_pop = {
				culture = merfolk
				size = 32000
			}
			create_pop = {
				culture = fangaulan
				size = 480000
			}
			create_pop = {
				culture = nalayni_harpy
				size = 22400
			}
		}
	}

	s:STATE_SSIPPANSEK = {
		region_state:L07 = {
			create_pop = {
				culture = ashamadi
				size = 198400
			}
			create_pop = {
				culture = adeeni_lizardfolk
				size = 40600
			}
			create_pop = {
				culture = imperial_lizardfolk
				size = 1001000
			}
		}
	}

	s:STATE_OLD_JINNAKAH = {
		region_state:L05 = {
			create_pop = {
				culture = suhratbi
				size = 11480
			}
			create_pop = {
				culture = guryadagga
				size = 10000
			}
			create_pop = {
				culture = merfolk
				size = 4920
			}
		}
		region_state:L19 = {
			create_pop = {
				culture = guryadagga
				size = 2600
			}
		}
		region_state:L16 = {
			create_pop = {
				culture = guryadagga
				size = 12000
			}
		}
	}

	s:STATE_QAYNSLAND = {
		region_state:L09 = {
			create_pop = {
				culture = ardimyan
				size = 437100
			}
			create_pop = {
				culture = guryadagga
				size = 32900
			}
		}
	}

	s:STATE_THE_OHITS = {
		region_state:L09 = {
			create_pop = {
				culture = ardimyan
				size = 691200
			}
			create_pop = {
				culture = guryadagga
				size = 28800
			}
		}
	}

	s:STATE_HARENMARCHES = {
		region_state:L10 = {
			create_pop = {
				culture = guryadagga
				size = 510000
			}
		}
	}

	s:STATE_SAMAANIA = {
		region_state:L11 = {
			create_pop = {
				culture = guryadagga
				size = 340000
			}
		}
	}

	s:STATE_ELIANDE = {
		region_state:L18 = {
			create_pop = {
				culture = guryadagga
				size = 2200
			}
		}
		region_state:L12 = {
			create_pop = {
				culture = vernman
				size = 97600
			}
			create_pop = {
				culture = guryadagga
				size = 40400
			}
		}
	}

	s:STATE_HADEADOL = {
		region_state:L12 = {
			create_pop = {
				culture = vernman
				size = 112200
			}
			create_pop = {
				culture = guryadagga
				size = 21000
			}
		}
		region_state:L13 = {
			create_pop = {
				culture = guryadagga
				size = 202100
			}
		}
		region_state:L14 = {
			create_pop = {
				culture = east_damerian
				size = 35700
			}
			create_pop = {
				culture = guryadagga
				size = 9000
			}
		}
	}

	s:STATE_HARASCILDE = {
		region_state:L15 = {
			create_pop = {
				culture = lorentish
				size = 108000
			}
			create_pop = {
				culture = guryadagga
				size = 61000
			}
		}
		region_state:L16 = {
			create_pop = {
				culture = guryadagga
				size = 281000
			}
		}
	}
}