﻿STATES = {

	s:STATE_MENGABOYS_1 = {
		create_state = {
			country = c:L01
			owned_provinces = { "x6E54CC" "x6FCE70" "x785BF1" "xACA054" "xFF6C4A" }
		}
		
		add_homeland = cu:yeteferen
	}

	s:STATE_MENGABOYS_2 = {
		create_state = {
			country = c:L01
			owned_provinces = { "x0676C1" "x584FA1" "x6E35D5" "x7F0A83" "x88FB53" "xA5BFE0" "xBBD931" "xBCA657" "xDC7E80" }
		}
		
		add_homeland = cu:metobesebi
	}

	s:STATE_MENGABOYS_3 = {
		create_state = {
			country = c:L01
			owned_provinces = { "x540C7D" "x61D33E" "x84D7E4" "xF1D14D" }
		}
		
		add_homeland = cu:yeteferen
	}

	s:STATE_MENGABOYS_4 = {
		create_state = {
			country = c:L01
			owned_provinces = { "x13646E" "x622856" "xAA77A5" "xDF9B79" "xFB1332" }
		}
		
		add_homeland = cu:metobesebi
	}

	s:STATE_MENGABOYS_5 = {
		create_state = {
			country = c:L01
			owned_provinces = { "x403E5E" "x5FD4D5" "xAD36BE" "xEB5927" "xFBF5D9" }
		}
		
		add_homeland = cu:metobesebi
	}

	s:STATE_MENGABOYS_6 = {
		create_state = {
			country = c:L01
			owned_provinces = { "x1A82FA" "x1F0E9A" "x22F1E8" "x8C568A" "xA7CBD3" }
		}
		
		add_homeland = cu:yeteferen
	}

	s:STATE_MENGABOYS_7 = {
		create_state = {
			country = c:L01
			owned_provinces = { "x29A665" "xA22D1A" "xEC93BF" "xF2D82F" "xFECD1B" }
		}
		
		add_homeland = cu:yeteferen
	}

	s:STATE_MENGABOYS_8 = {
		create_state = {
			country = c:L01
			owned_provinces = { "x01B257" "x0A6F45" "x734BD5" "x860DB0" "xFC2598" }
		}
		
		add_homeland = cu:yeteferen
	}

	s:STATE_MENGABOYS_9 = {
		create_state = {
			country = c:L01
			owned_provinces = { "x04685C" "x0549B4" "x37B05D" "x6DB719" }
		}
		
		add_homeland = cu:yeteferen
	}

	s:STATE_MENGABOYS_10 = {
		create_state = {
			country = c:L01
			owned_provinces = { "x59FB7C" "x5FCE17" "xD69592" }
		}
		
		add_homeland = cu:yeteferen
	}

	s:STATE_MENGABOYS_11 = {
		create_state = {
			country = c:L01
			owned_provinces = { "x37F3F7" "x449A3F" "x5033FD" "xC3551E" "xE89730" }
		}
		
		add_homeland = cu:yeteferen
		add_homeland = cu:sitewosi
	}

	s:STATE_MENGABOYS_12 = {
		create_state = {
			country = c:L01
			owned_provinces = { "x402D48" "x4CB0E9" "x729E3A" "x961679" "xB77C6C" "xD129E0" "xE5B366" "xE7621C" }
		}
		
		add_homeland = cu:sitewosi
	}

	s:STATE_MENGABOYS_13 = {
		create_state = {
			country = c:L01
			owned_provinces = { "x29F897" "x4AE859" "x50E2C4" "x784853" "x847F34" "xB56832" "xDB6A3E" "xF28AEF" }
		}
		
		add_homeland = cu:sitewosi
	}

	s:STATE_MENGABOYS_14 = {
		create_state = {
			country = c:L01
			owned_provinces = { "x0D8A44" "x23F3F8" "x3A6103" "x5C452C" "x7E3A04" "xC5C652" "xDBA322" }
		}
		
		add_homeland = cu:sitewosi
	}

	s:STATE_MENGABOYS_15 = {
		create_state = {
			country = c:L01
			owned_provinces = { "x07A3E9" "x454B41" "xA0F05E" "xCC340B" }
		}
		
		add_homeland = cu:talilibeti
	}

	s:STATE_MENGABOYS_16 = {
		create_state = {
			country = c:L01
			owned_provinces = { "x0FA71C" "x293DEC" "x67077B" "x830696" "xA491D1" }
		}
		
		add_homeland = cu:ofehibi
	}

	s:STATE_MENGABOYS_17 = {
		create_state = {
			country = c:L01
			owned_provinces = { "x28E5B2" "x2B095D" "x4914EB" "x6FFF71" "x9C379A" }
		}
		
		add_homeland = cu:ofehibi
	}

	s:STATE_MENGABOYS_18 = {
		create_state = {
			country = c:L01
			owned_provinces = { "x1A5F2C" "x8ABD44" "x99BA2C" "xA37227" "xA41079" "xAFC9C2" "xEBCBB4" }
		}
		
		add_homeland = cu:talilibeti
	}

	s:STATE_MENGABOYS_19 = {
		create_state = {
			country = c:L03
			owned_provinces = { "x2CB55D" "x4133AF" "x838734" "xD90AD8" "xE08608" "xE616E0" }
		}
		
		add_homeland = cu:ofehibi
	}

	s:STATE_MENGABOYS_20 = {
		create_state = {
			country = c:L04
			owned_provinces = { "x202FE4" "x38570E" "x3C816A" "x40A58B" "x4D7623" "x65580B" "xAE0510" "xB40676" "xCB0905" "xD656F8" "xF8B528" }
		}
		
		add_homeland = cu:ofehibi
		add_homeland = cu:nalayni_harpy
	}

	s:STATE_MENGABOYS_22 = {
		create_state = {
			country = c:L02
			owned_provinces = { "x0370B1" "x068294" "x4007CF" "x47C2B5" "x785838" "x863213" "x9E56D5" "xA5245A" "xD2C2A2" }
		}
		
		add_homeland = cu:ofehibi
	}

	s:STATE_MENGABOYS_23 = {
		create_state = {
			country = c:L02
			owned_provinces = { "x21C393" "x74C950" "xC68C55" "xEF8527" "xF5777A" }
		}
		
		add_homeland = cu:ofehibi
	}

	s:STATE_MENGABOYS_24 = {
		create_state = {
			country = c:L03
			owned_provinces = { "x047343" "x336593" "x901BFA" "xC3E1AB" "xE15E0E" "xEF0704" "xF6DF9F" }
		}
		
		add_homeland = cu:ofehibi
	}

	#Haraag

	s:STATE_MENGABOYS_25 = {
		create_state = {
			country = c:L03
			owned_provinces = { "x0D06A1" "x2E13B5" "x458F49" "x64449C" "xA314A1" "xA7E304" "xD469F4" }
		}
		
		add_homeland = cu:maqeti
	}

	s:STATE_MENGABOYS_26 = {
		create_state = {
			country = c:L03
			owned_provinces = { "x27A59F" "x2D1419" "x3A74C4" "x829553" "x91BD0D" "xE58E1A" "xEFAE44" "xFDF3E1" }
		}
		
		add_homeland = cu:fieldstalker_gnoll
	}

	s:STATE_MENGABOYS_27 = {
		create_state = {
			country = c:L03
			owned_provinces = { "x0C0E29" "x2ED580" "x308BAF" "x5255C7" "x6A0A8C" "x725A54" "x875830" "x9657A9" "x9A1713" "xA55F32" "xA87CD2" "xBB46E7" "xC2C916" }
		}
		
		add_homeland = cu:fieldstalker_gnoll
	}

	s:STATE_MENGABOYS_28 = {
		create_state = {
			country = c:L03
			owned_provinces = { "x6AB135" "x6F9647" "xA466F0" "xA87D9D" "xC99ABA" "xFE8F9F" }
		}
		
		add_homeland = cu:fieldstalker_gnoll
	}

	s:STATE_MENGABOYS_29 = {
		create_state = {
			country = c:F16	#Kheterata
			owned_provinces = { "x06AC72" "x083CFD" "x110642" "x164EB0" "x59EB87" "x6D1E24" "x87E151" "xB0244A" "xCEF8E8" "xD25C03" "xDCCFEA" "xE185B2" "xF3DF24" }
		}
		
		add_homeland = cu:fieldstalker_gnoll
	}


	#Shadow Swamp Yezel Mora
	s:STATE_MENGABOYS_30 = {
		create_state = {
			country = c:L01
			owned_provinces = { "x0102FA" "x0142A9" "x07C9FE" "x1BF2FF" "x57AF64" "x604664" "x64400F" "x9BDB12" "xA3A6D5" "xC73232" "xF1FBE7" "xFF92AD" }
		}
		
		add_homeland = cu:swamp_troll
	}

	s:STATE_MENGABOYS_31 = {
		create_state = {
			country = c:L01
			owned_provinces = { "x13C840" "x185E4B" "x1ECDB6" "x2ACCB9" "x65CA96" "x99C3CB" "xC00C25" "xCE4592" }
		}
		
		add_homeland = cu:swamp_troll
	}

	s:STATE_MENGABOYS_32 = {
		create_state = {
			country = c:L01
			owned_provinces = { "x055B06" "x074C4D" "x31600A" "x3602CC" "x452CD6" "x55D154" "x634B14" "xA22D60" "xA45E7E" "xCD0740" "xCD69F5" }
		}
		
		add_homeland = cu:swamp_troll
	}

	s:STATE_MENGABOYS_33 = {
		create_state = {
			country = c:L01
			owned_provinces = { "x12D63F" "x175AFE" "x1CC813" "x39F374" "x813426" "x854A53" "x9D504B" "xB8F070" "xD20581" "xF5B47D" "xFDB661" }
		}
		
		add_homeland = cu:swamp_troll
	}

	s:STATE_LIZARDFOLK_1 = { 
		create_state = {
			country = c:L20
			owned_provinces = { "x1927F2" "x3458EA" "x6CB2AC" "x6F03E2" "x7502F7" "x799A8D" "x9BD28A" "xAA4132" "xAB0B4E" "xAB5FEB" "xC135CB" "xE40C7C" }
		}
		
		add_homeland = 
	}
	s:STATE_LIZARDFOLK_2 = { 
		create_state = {
			country = c:L20
			owned_provinces = {  "x15F8DE" "x5457A7" "x551208" "x5C8DFA" "x832455" "x903D13" "x90B9EF" "xC929AF" "xD00D66" }
		}
		
		add_homeland = 
	}
	s:STATE_LIZARDFOLK_3 = { 
		create_state = {
			country = c:L20
			owned_provinces = { "x044BF3" "x115C75" "x231782" "x28089B" "x432115" "x465C56" "x4C1B13" "x561B89" "x5743B9" "x5D1D9A" "x6DB940" "x77E5E0" "x8C2593" "x8F4CE1" "x937036" "x9FB8FE" "xA681D7" "xAC9807" "xB4DCC4" "xBBD7EC" "xD55571" "xD9744E" "xE6EAF3" "xEEF99B" }
		}
		
		add_homeland = 
	}
	s:STATE_LIZARDFOLK_4 = { 
		create_state = {
			country = c:L20
			owned_provinces = { "x20566B" "x3C54B0" "x4F5410" "x5DE532" "x6CFC28" "x8E9C72" "xCB6403" "xDEDF3B" "xF5C72F" "xF82DC9" }
		}
		
		add_homeland = 
	}
	s:STATE_LIZARDFOLK_5 = { 
		create_state = {
			country = c:L20
			owned_provinces = { "x0FE5AF" "x13E98D" "x1E6C9F" "x2DCD84" "x466C49" "x48B296" "x5021A2" "xC25C95" "xE09953" "xE5B3B5" "xE720F5" "xED90B9" "xF97261" }
		}
		
		add_homeland = 
	}
	s:STATE_LIZARDFOLK_6 = { 
		create_state = {
			country = c:L20
			owned_provinces = { "x04456B" "x0ACBF0" "xA487F1" "xB43223" "xCB83AA" "xFF1E08" }
		}
		
		add_homeland = 
	}
	s:STATE_LIZARDFOLK_7 = { 
		create_state = {
			country = c:L20
			owned_provinces = { "x27BBE1" "x2B0C7F" "x434E19" "x4F1337" "x60BA3A" "x653F94" "x7131CC" "x7A6BD1" "x97C61D" "xC98245" "xCDE9C3" "xF5532C" }
		}
		
		add_homeland = 
	}
	s:STATE_LIZARDFOLK_8 = { 
		create_state = {
			country = c:L20
			owned_provinces = { "x091BC1" "x0CE5D6" "x35A361" "x3EBB01" "x6BB684" "x6C8AE0" "x773071" "x93170A" "x9BACE5" "xE98BEB" }
		}
		
		add_homeland = 
	}
	s:STATE_LIZARDFOLK_9 = { 
		create_state = {
			country = c:L20
			owned_provinces = { "x1737C4" "x20B4B2" "x31455A" "x612C58" "x756CE6" "x9F42E8" "xA49011" "xE218E1" }
		}
		
		add_homeland = 
	}
	s:STATE_LIZARDFOLK_10 = { 
		create_state = {
			country = c:L20
			owned_provinces = { "x065652" "x0787B9" "x104785" "x13F8AE" "x3C5388" "x527AA4" "x66EE9B" "x8A756A" "xA949C5" "xC2FD33" "xC5E6C0" "xCCAED4" }
		}
		
		add_homeland = 
	}
	s:STATE_LIZARDFOLK_11 = { 
		create_state = {
			country = c:L21
			owned_provinces = { "x1080FA" "x222075" "x3D2EE3" "x509A66" "x57F214" "x842B61" "x854479" "x87BF62" "x8ED460" "x99F64C" "xBBD01B" "xBF7674" "xC485DD" }
		}
		
		add_homeland = 
	}
	s:STATE_LIZARDFOLK_12 = { 
		create_state = {
			country = c:L21
			owned_provinces = { "x1A8E5D" "x4305F7" "x5D1CC7" "x6F6188" "x820BAC" "x8668FA" "x89F9CB" "x9103F4" "xC1F133" "xC5027C" "xCD9120" }
		}
		
		add_homeland = 
	}
	s:STATE_LIZARDFOLK_13 = { 
		create_state = {
			country = c:L21
			owned_provinces = { "x0111A5" "x20DFF1" "x50C7A3" "x786C08" "x8A5138" "xAC74A1" }
		}
		
		add_homeland = 
	}
	s:STATE_LIZARDFOLK_14 = { #Split state
		create_state = {
			country = c:L21
			owned_provinces = { "xD6FB95" "xFFA5BA" }
		}
		
		add_homeland = 
	}
	s:STATE_LIZARDFOLK_14 = { #Split state
		create_state = {
			country = c:L06
			owned_provinces = { "x1E72D7" }
		}
		
		add_homeland = 
	}
	s:STATE_LIZARDFOLK_15 = { 
		create_state = {
			country = c:L21
			owned_provinces = { "x1CFB9C" "x337512" "x49EE6D" "x510B6A" "x6BD65C" "x9CC172" "xD25350" "xD32FDB" }
		}
		
		add_homeland = 
	}
	s:STATE_LIZARDFOLK_16 = { 
		create_state = {
			country = c:L21
			owned_provinces = { "x3E3049" "x41B615" "x455D6D" "x48BA21" "x7B8FE4" "xA5E523" "xB6F0A0" }
		}
		
		add_homeland = 
	}
	s:STATE_LIZARDFOLK_17 = {
		create_state = {
			country = c:L21
			owned_provinces = { "x24C462" "x3809DB" "x3F10F6""x72AF42" "x9E77BF" "xAA280F" "xD8DFFB" }
		}
		
		add_homeland = 
	}

}