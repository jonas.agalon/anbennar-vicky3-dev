﻿ideology_racial_purist = {
	icon = "gfx/interface/icons/ideology_icons/ideology_leader/ideology_leader_ethno_nationalist.dds" 
	
	character_ideology = yes

	lawgroup_racial_tolerance = {
		law_same_race_only = strongly_approve
		law_same_race_and_humans = approve
		law_giantkin_group_only = approve
		law_ruinborn_group_only = approve
		law_goblinoid_group_only = approve
		law_giantkin_group_and_humans = neutral
		law_ruinborn_group_and_humans = neutral
		law_goblinoid_group_and_humans = neutral
		law_monstrous_only = disapprove
		law_non_monstrous_only = disapprove
		law_all_races_allowed = strongly_disapprove	
	}
	
	# Don't want monsters migrating
	lawgroup_migration = {
		law_migration_controls = strongly_approve
		law_closed_borders = approve
		law_no_migration_controls = disapprove	
	}

	# TODO
	possible = {
		# owner = {
			# NOT = { has_technology_researched = nationalism }
		# }
		scope:interest_group = {
			NOR = {
				has_variable = chose_vanguardism
				has_variable = communist_ig_var
				has_variable = chose_fascism
				has_variable = chose_conservatism
				has_variable = chose_ethno_nationalism
			}
		}
	}
	
	leader_weight = {
		value = 25
		# More likely with a convenient scapegoat and is angry without same race only
		if = {
			limit = {
				owner = {
					NOT = { has_law = law_type:law_same_race_only }
					any_scope_state = {
						is_incorporated = yes
						any_scope_pop = {
							NOT = { 
								pop_race_accepted = {
									COUNTRY = owner
								}
							}
						}
					}
				}
				scope:interest_group = {
					ig_approval <= -5
				}
			}
			add = 25
		}
		# More likely if angry and issues are relevant
		if = {
			limit = {
				owner = {
					NOR = {
						has_law = law_type:law_same_race_only
						has_law = law_type:law_same_race_and_humans
						has_law = law_type:law_giantkin_group_only
						has_law = law_type:law_ruinborn_group_only
						has_law = law_type:law_goblinoid_group_only
					}
				}
				scope:interest_group = {
					ig_approval <= -5
				}
			}
			add = 50
		}
		# Less likely in council republic
		if = {
			limit = {
				owner = {
					has_law = law_type:law_council_republic
				}
			}
			add = -25
		}
		# less likely if content without same race only
		if = {
			limit = {
				owner = {
					NOT = {
						has_law = law_type:law_same_race_only
					}
				}
				scope:interest_group = {
					ig_approval <= 5
				}
			}
			add = -25
		}
	}
}

#TODO link this into adventuring laws?
ideology_adventurer = {
	icon = "gfx/interface/icons/ideology_icons/ideology_leader/ideology_leader_ethno_nationalist.dds" 
	
	character_ideology = yes

	#Like laws that ban monsters
	lawgroup_racial_tolerance = {
		law_same_race_only = approve
		law_giantkin_group_only = disapprove
		law_goblinoid_group_only = disapprove
		law_giantkin_group_and_humans = disapprove
		law_goblinoid_group_and_humans = disapprove
		law_monstrous_only = strongly_disapprove
		law_non_monstrous_only = strongly_approve
		law_all_races_allowed = strongly_disapprove
	}
	
	# Want to be able to move about to monsters and stuff
	lawgroup_migration = {
		law_migration_controls = strongly_approve
		law_closed_borders = disapprove
		law_no_migration_controls = disapprove	
	}
	
	#Only if your country has any non-monsters in it and the character isn't a monster
	possible = {
		owner = {
			any_primary_culture = {
				is_non_monstrous_culture = yes
			}
		}
		this.culture = {
			is_non_monstrous_culture = yes
		}
		
		scope:interest_group = {
			NOR = {
				has_variable = chose_vanguardism
				has_variable = communist_ig_var
				has_variable = chose_fascism
				has_variable = chose_conservatism
				has_variable = chose_ethno_nationalism
			}
		}
	}
	
	leader_weight = {
		value = 25
		# less likely among more progressive IG's if they're angry and socialism is researched
		if = {
			limit = {
				owner = {
					has_technology_researched = socialism
				}
				scope:interest_group = {
					OR = {
						is_interest_group_type = ig_intelligentsia
						is_interest_group_type = ig_trade_unions
					}
					ig_approval < 0
				}
			}
			add = -75
		}
		# more likely when issues are relevant
		if = {
			limit = {
				owner = {
					OR = {
						has_law = law_type:law_all_races_allowed
						has_law = law_type:law_monstrous_only
					}
				}
			}
			add = 50
		}
		#Add stuff about adventuring laws here?
	}
}

#Monster version of adventurer, needs a new name
ideology_monstrous_adventurer = {
	icon = "gfx/interface/icons/ideology_icons/ideology_leader/ideology_leader_ethno_nationalist.dds" 
	
	character_ideology = yes

	#Like laws that ban non-monsters, no humans!
	lawgroup_racial_tolerance = {
		law_same_race_only = approve
		law_same_race_and_humans = disapprove
		law_giantkin_group_and_humans = disapprove
		law_goblinoid_group_and_humans = disapprove
		law_monstrous_only = strongly_approve
		law_non_monstrous_only = strongly_disapprove
		law_all_races_allowed = strongly_disapprove
	}
	
	# Want to be able to move about to monsters and stuff
	lawgroup_migration = {
		law_migration_controls = strongly_approve
		law_closed_borders = disapprove
		law_no_migration_controls = disapprove	
	}
	
	# Only if your country has any monsters running it and the character is a monster
	possible = {
		owner = {
			any_primary_culture = {
				is_monstrous_culture = yes
			}
		}
		this.culture = {
			is_monstrous_culture = yes
		}
		
		scope:interest_group = {
			NOR = {
				has_variable = chose_vanguardism
				has_variable = communist_ig_var
				has_variable = chose_fascism
				has_variable = chose_conservatism
				has_variable = chose_ethno_nationalism
			}
		}
	}
	
	leader_weight = {
		value = 25
		# less likely among more progressive IG's if they're angry and socialism is researched
		if = {
			limit = {
				owner = {
					has_technology_researched = socialism
				}
				scope:interest_group = {
					OR = {
						is_interest_group_type = ig_intelligentsia
						is_interest_group_type = ig_trade_unions
					}
					ig_approval < 0
				}
			}
			add = -75
		}
		# more likely when issues are relevant
		if = {
			limit = {
				owner = {
					OR = {
						has_law = law_type:law_all_races_allowed
						has_law = law_type:law_non_monstrous_only
					}
				}
			}
			add = 50
		}
		#Add stuff about adventuring laws here?
	}
}

# Wants EVIL artifice
ideology_mad_scientist = {
	icon = "gfx/interface/icons/ideology_icons/ideology_leader/ideology_leader_ethno_nationalist.dds" 
	
	character_ideology = yes
	
	lawgroup_artificer_ethics = {
		law_amoral_artifice_banned = disapprove
		law_pragmatic_artifice = neutral
		law_amoral_artifice_embraced = approve
	}
	
	lawgroup_magic_and_artifice = {
		law_mundane_production = strongly_disapprove
		law_traditional_magic_only = strongly_disapprove
		law_traditional_magic_encouraged = neutral
		law_artifice_encouraged = approve
		law_artifice_only = approve
	}
	
	# Only if your country allows artifice
	possible = {
		owner = {
			NOR = {
				has_law = law_type:law_traditional_magic_only
				has_law = law_type:law_mundane_production
			}
		}
		
		scope:interest_group = {
			NOR = {
				has_variable = chose_vanguardism
				has_variable = communist_ig_var
				has_variable = chose_fascism
				has_variable = chose_conservatism
				has_variable = chose_ethno_nationalism
			}
		}
	}
	
	leader_weight = {
		value = 50
		# Less likely if from a "traditional" IG
		if = {
			limit = {
				scope:interest_group = {
					OR = {
						is_interest_group_type = ig_landowners
						is_interest_group_type = ig_devout
					}
				}
			}
			add = -75
		}
		# more likely when issues are relevant
		if = {
			limit = {
				owner = {
					OR = {
						has_law = law_type:law_amoral_artifice_banned
						has_law = law_type:law_pragmatic_artifice
					}
				}
			}
			add = 50
		}
	}
}

ideology_magocrat = {
	# icon = "gfx/interface/icons/ideology_icons/ideology_leader/ideology_leader_theocrat.dds"
	
	character_ideology = yes

	lawgroup_governance_principles = {
		law_magocracy = strongly_approve	
		law_monarchy = neutral
		law_theocracy = disapprove	
		law_presidential_republic = disapprove
		law_parliamentary_republic = disapprove
		law_council_republic = disapprove
	}
	
	possible = {
		OR = {
			owner = {
				has_law = law_type:law_magocracy
			}
			scope:interest_group = {
				is_interest_group_type = ig_landowners
			}
		}
		scope:interest_group = {
			OR = {
				is_interest_group_type = ig_armed_forces
				is_interest_group_type = ig_petty_bourgeoisie
				is_interest_group_type = ig_landowners
				is_interest_group_type = ig_intelligentsia
			}
		}
		scope:interest_group = {
			NOR = {
				has_variable = chose_vanguardism
				has_variable = communist_ig_var
				has_variable = chose_fascism
				has_variable = chose_conservatism
				has_variable = chose_ethno_nationalism
			}
		}
	}
	
	leader_weight = {
		value = 100
		# less likely in a theocracy
		if = {
			limit = {
				owner = {
					has_law = law_type:law_theocracy
				}
			}
			add = -75
		}
		# less likely if IG is happy in a republic
		if = {
			limit = {
				owner = {
					NOT = { has_law = law_type:law_magocracy }
				}
				scope:interest_group = {
					ig_approval > 0
				}
			}
			add = -100
		}
		# more likely if IG is happy in a magocracy
		if = {
			limit = {
				owner = {
					has_law = law_type:law_magocracy
				}
				scope:interest_group = {
					ig_approval > 5
				}
			}
			add = 50
		}
	}
}

ideology_aldanist = {
	# icon = "gfx/interface/icons/ideology_icons/ideology_leader/ideology_leader_corporatist.dds"
	
	character_ideology = yes

	lawgroup_citizenship = {
		law_ethnostate = strongly_disapprove
		law_national_supremacy = neutral
		law_racial_segregation = approve
		law_cultural_exclusion = disapprove
		law_multicultural = strongly_disapprove
	}

	lawgroup_racial_tolerance = {
		law_same_race_only = strongly_approve
		law_non_monstrous_only = disapprove
		law_all_races_allowed = disapprove
	}
	
	possible = {
		is_ruinborn = yes
		scope:interest_group = {
			OR = {
				is_interest_group_type = ig_armed_forces
				is_interest_group_type = ig_petty_bourgeoisie
				is_interest_group_type = ig_landowners
				is_interest_group_type = ig_intelligentsia
			}
		}
		#add further restrictions later
	}
	
	leader_weight = {
		value = 50
		#TODO: if IG angry and ethno/supremacy/multi + more likely if lots of discriminated ruinborn?
	}
}

ideology_venaanist = {
	# icon = "gfx/interface/icons/ideology_icons/ideology_leader/ideology_leader_communist.dds"
	
	character_ideology = yes

	lawgroup_citizenship = {
		law_ethnostate = strongly_disapprove
		law_national_supremacy = strongly_disapprove
		law_racial_segregation = disapprove
		law_cultural_exclusion = neutral
		law_multicultural = approve
	}

	lawgroup_racial_tolerance = {
		law_same_race_only = strongly_disapprove
		law_non_monstrous_only = neutral
		law_all_races_allowed = neutral
	}
	
	possible = {
		is_ruinborn = yes
		scope:interest_group = {
			OR = {
				is_interest_group_type = ig_armed_forces
				is_interest_group_type = ig_intelligentsia
			}
		}
		#add further restrictions later
	}
	
	leader_weight = {
		value = 50
		#TODO: rarer than aldanist
	}
}