﻿# group = this is the law_group a law belongs to
# icon = graphical icon shown in-game
# modifier = {} modifier on country for having adopted this law

law_magocracy = {
	group = lawgroup_governance_principles
	
	# icon = "gfx/interface/icons/law_icons/monarchy.dds"
	
	progressiveness = -50

	modifier = {
		country_legitimacy_headofstate_add = 20
		country_legitimacy_govt_total_clout_add = 0.10
		country_mages_pol_str_mult = 0.5
	}
	
	possible_political_movements = {
		law_monarchy
		law_presidential_republic
		law_parliamentary_republic
		law_council_republic
	}
	
	ai_will_do = {
		exists = ruler
		ruler = {
			has_ideology = ideology:ideology_magocrat
		}
	}
}