﻿## Farms
pm_mass_cast_plant_growth_farms = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_reagents_add = 5				
			
			# output goods													
			goods_output_grain_add = 20
		}

		level_scaled = {
			# earnings														
			building_employment_mages_add = 250
		}
	}
}

pm_spirit_imbuement_farms = {
	texture = "gfx/interface/icons/production_method_icons/soil_enriching_farming.dds"

	unlocking_technologies = {
		intensive_agriculture
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_spirit_energy_add = 10
			goods_input_fertilizer_add = 5		
			
			# output goods													
			goods_output_grain_add = 40
		}

		level_scaled = {
			# earnings														
			building_employment_laborers_add = -500
			building_employment_machinists_add = 500
		}
	}
}

pm_growth_serum_farms = {
	texture = "gfx/interface/icons/production_method_icons/fertilization.dds"
	
	unlocking_technologies = {
		improved_fertilizer
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_spirit_energy_add = 20
			goods_input_fertilizer_add = 5
			goods_input_artificery_doodads_add = 5		
			
			# output goods													
			goods_output_grain_add = 80
		}

		level_scaled = {
			# earnings														
			building_employment_laborers_add = -750
			building_employment_machinists_add = 500
			building_employment_engineers_add = 250
		}
	}
}

pm_automata_harvesters = {
	texture = "gfx/interface/icons/production_method_icons/steam_powered_threshers.dds"

	unlocking_technologies = {
		threshing_machine
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods													
			goods_input_automata_add = 4
		}

		level_scaled = {
			building_employment_laborers_add = -3000
		}
	}
}

pm_autonomous_tractors = {
	texture = "gfx/interface/icons/production_method_icons/tractors.dds"

	unlocking_technologies = {
		mechanized_farming
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods								
			goods_input_automata_add = 7
		}

		level_scaled = {
			building_employment_laborers_add = -3000
			building_employment_machinists_add = -500
			building_employment_farmers_add = -200
		}
	}
}

## Livestock
pm_polymorphed_megasheep = {
	texture = "gfx/interface/icons/production_method_icons/large_sheep_ranch.dds"

	unlocking_technologies = {
		mechanized_farming
	}

	unlocking_production_methods = {
		pm_slaughterhouses
		pm_mechanized_slaughtering
		pm_perpetual_meat_husks
	}
	
	building_modifiers = {
		workforce_scaled = {
			goods_input_grain_add = 30
			goods_input_spirit_energy_add = 5
			
			
			goods_output_fabric_add = 65
			goods_output_fertilizer_add = 25
			goods_output_meat_add = -25
		}
	}
}

pm_soul_battery_sheep = {
	texture = "gfx/interface/icons/production_method_icons/large_sheep_ranch.dds"

	unlocking_technologies = {
		mechanized_farming
	}

	unlocking_production_methods = {
		pm_slaughterhouses
		pm_mechanized_slaughtering
		pm_perpetual_meat_husks
	}
	
	building_modifiers = {
		workforce_scaled = {
			goods_input_grain_add = 30
			
			goods_output_fabric_add = 30
			goods_output_fertilizer_add = 10
			goods_output_meat_add = -10
			goods_output_spirit_energy_add = 10
		}
	}
}

pm_perpetual_meat_husks = {
	texture = "gfx/interface/icons/production_method_icons/mechanized_slaughtering.dds"	

	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 15
		}
	}

	unlocking_technologies = {
		mechanized_farming
	}
	building_modifiers = {
		workforce_scaled = {
			goods_input_tools_add = 10
			goods_input_spirit_energy_add = 10
			
			goods_output_meat_add = 65
		}

		level_scaled = {
			building_employment_laborers_add = 2750
			building_employment_farmers_add = 900
			building_employment_clergymen_add = 200
			building_employment_machinists_add = 900
			building_employment_engineers_add = 250
		}
	}
}

pm_geas_husbandry = {
	texture = "gfx/interface/icons/production_method_icons/electric_fencing.dds"	
	unlocking_technologies = {
		electrical_generation	
	}
	building_modifiers = {
		workforce_scaled = {
			goods_input_artificery_doodads_add = 10
		}

		level_scaled = {
			building_employment_laborers_add = -1600
			building_employment_clergymen_add = -50
			building_employment_machinists_add = -900
		}
	}

	required_input_goods = electricity
}

pm_ensouled_preservation_building_livestock_ranch = {
	texture = "gfx/interface/icons/production_method_icons/refrigerated_storage.dds"

	building_modifiers = {
		workforce_scaled = {
			goods_input_spirit_energy_add = 5

			goods_output_meat_add = 5
		}

		level_scaled = {
			building_employment_laborers_add = -800
		}
	}
}

pm_regenerative_preservation_building_livestock_ranch = {
	texture = "gfx/interface/icons/production_method_icons/refrigerated_storage.dds"

	building_modifiers = {
		workforce_scaled = {
			goods_input_electricity_add = 5
			goods_input_transportation_add = 5
			goods_input_spirit_energy_add = 5

			goods_output_meat_add = 10
		}

		level_scaled = {
			building_employment_laborers_add = -3000
		}
	}
	required_input_goods = electricity
}

