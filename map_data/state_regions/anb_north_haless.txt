﻿STATE_429 = {
    id = 429
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x050D8E" "x071CF7" "x0C69F3" "x0C9A39" "x0E784F" "x149479" "x179196" "x1943E1" "x1955A8" "x1A4267" "x1C59CC" "x232E1F" "x29DFC3" "x2B0CF4" "x2B50AB" "x32C2B3" "x3505A1" "x363D4E" "x39E9BC" "x3B56F3" "x3FE668" "x444EFC" "x468D1D" "x46E6ED" "x4BFDA4" "x5040B0" "x50C0B0" "x50F19B" "x51B446" "x523A6E" "x586231" "x5A7F48" "x5ACC0F" "x60236D" "x6480D7" "x666A36" "x6A5DDE" "x738A48" "x80C5CC" "x8385E4" "x8C50BF" "x8CB88A" "x8EE07C" "x8F6070" "x92A994" "x96DD26" "x998BCA" "x9C60C9" "x9CAE58" "x9FA2F1" "xA18C1B" "xA1C4D2" "xAD2D38" "xB1500E" "xB8978D" "xC5AD14" "xC6687D" "xD040B0" "xD0FA16" "xD46C31" "xD51A40" "xDFB440" "xE2064F" "xE353A8" "xE7C924" }
    traits = {}
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 52
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 3308 #Hokhos Sea
}
STATE_430 = {
    id = 430
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x0CF2F2" "x0F5DA3" "x1E598E" "x25D013" "x281A56" "x2ADF0A" "x31D7D1" "x3EBED3" "x48155A" "x4C36B5" "x4E852C" "x6DB35E" "x71B813" "x736B75" "x878CA0" "x97429C" "x9F8945" "xA1D681" "xAA85A9" "xB58FBC" "xB94139" "xB9639B" "xBD4EAC" "xC1A6D9" "xC494E7" "xC63C98" "xC68C0C" "xC7C6C5" "xC7D55A" "xD10BCA" "xDC85EB" "xDD304F" "xDD6E44" "xE1AFCE" "xE3D724" "xF2A7A4" "xF528D7" "xFFD548" }
    traits = {}
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 56
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
        bg_whaling = 1
    }
    naval_exit_id = 3307 #Widow's Sea
}
STATE_431 = {
    id = 431
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x00E563" "x00FADB" "x03B866" "x04BA50" "x05C4AE" "x10CA9A" "x1138C4" "x1299E9" "x134273" "x157160" "x192BC5" "x1BB014" "x1F97B1" "x223E7E" "x23C2FB" "x2AA788" "x331045" "x3459B4" "x365AA4" "x385ED7" "x450261" "x48AD17" "x51B1A0" "x537223" "x582A47" "x58318D" "x5A611F" "x657D76" "x6A85B8" "x6CE4EF" "x6F62E4" "x7153B0" "x78B6A5" "x7AD8FD" "x82EE1E" "x8546D1" "x8999AB" "x8B2D26" "x8F5977" "x8F8497" "x933044" "x9C9CE7" "x9F7B68" "xA03A8F" "xA3D656" "xA5478F" "xAA5795" "xB11298" "xB3A718" "xB57B49" "xB5C6C3" "xC4BFE4" "xC5F57C" "xC9B915" "xCB498A" "xCC7E11" "xCF0D01" "xD26788" "xD3A211" "xD5C6CE" "xDD04A3" "xE161CA" "xE46BA4" "xF7C368" "xFC54A3" }
    traits = {}
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 20
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}
STATE_432 = {
    id = 432
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x06FF7B" "x101EFD" "x142D60" "x194C37" "x1A3BA9" "x1C8604" "x208C92" "x235ED2" "x2C38F7" "x36AD45" "x3C3F55" "x41FDEE" "x4828E3" "x4A92BA" "x4C06F2" "x4C7465" "x53BF4D" "x5860DC" "x5CEA19" "x6A6DC3" "x7511A0" "x788681" "x7B7DF0" "x7C78B5" "x8A7D0F" "x8B53EB" "x8C5291" "x904A84" "xA72BC6" "xA7A415" "xB0C90A" "xB94AB4" "xBAE43D" "xC4E032" "xC851CA" "xC9C2EC" "xCEF332" "xD2B00C" "xDF3C84" "xE151F9" "xEB1CE9" "xEB759F" "xF0CDB3" }
    traits = {}
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 37
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 3307 #Widow's Sea
}
STATE_433 = {
    id = 433
    subsistence_building = "building_subsistence_farms"
    provinces = { "x02140E" "x0FF874" "x117C2E" "x161A2E" "x1BDEFE" "x1CC093" "x27F74C" "x2873C8" "x2B09E1" "x2B2A60" "x2B6A32" "x2EAFA2" "x34AFA2" "x3F392E" "x41D39B" "x42AE95" "x449E22" "x44CDBE" "x44EDE0" "x44EF2B" "x4A6604" "x4D5F7D" "x551BE7" "x644AC5" "x6588E8" "x6795DD" "x6AA2FB" "x6CCED9" "x6E306E" "x704954" "x75C0D2" "x80D80D" "x82D14D" "x85F098" "x889061" "x8B792E" "x8E7B74" "x938B68" "x944EAF" "x9669C2" "x9B2D5D" "x9D91B7" "xA08BF0" "xA4D228" "xA5BADA" "xA70C0F" "xB53926" "xBABAB5" "xBD6561" "xBEA485" "xC565B4" "xC57886" "xC5BD16" "xC9FB10" "xCF885E" "xD6D2C9" "xD6EE50" "xDFFD26" "xE32849" "xE49A25" "xEB0D15" "xED4743" "xF7344C" "xFD6B8B" "xFDADCD" }
    traits = {}
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 56
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    naval_exit_id = 3307 #Widow's Sea
}
STATE_434 = {
    id = 434
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x004B8C" "x00BAC5" "x00EE11" "x042C59" "x061168" "x0AB49F" "x0B77F4" "x0DCF8D" "x0EAAEB" "x11CDB2" "x13E7CB" "x150BF0" "x18827F" "x1980E7" "x19CF99" "x1A2DCD" "x1A5631" "x1C1A1C" "x1C3658" "x1E7C49" "x203701" "x21C588" "x21D54E" "x23A0CB" "x23B2D7" "x261B30" "x293F4F" "x2B6C15" "x2B700B" "x2F1ABE" "x2F1C87" "x313A48" "x32493C" "x324EE5" "x329EF6" "x33BA2A" "x342752" "x34C7D3" "x374549" "x38AB7F" "x38FC9C" "x39FC44" "x3AF996" "x3BB30F" "x40EA4E" "x42D742" "x436FC7" "x4374A6" "x441EB3" "x4486F7" "x44AEB2" "x47AA5E" "x47BF69" "x4951FE" "x499773" "x49E39B" "x4A479E" "x4AB206" "x4AE131" "x4BC7CC" "x4CFB0E" "x50E40D" "x51C909" "x52605E" "x527982" "x545407" "x54AB06" "x563C5F" "x57BD15" "x58C434" "x5911D3" "x59FE16" "x5A6849" "x601503" "x617C58" "x6688D3" "x67CA7B" "x69A096" "x6C9CC2" "x6CAF72" "x6DD0F6" "x6EF01C" "x6F8624" "x7062F5" "x71A09D" "x71C984" "x71EFBD" "x721D56" "x754705" "x770627" "x77EF54" "x79997E" "x7E2416" "x7E540E" "x7F903D" "x7FB0DE" "x81029F" "x818FA8" "x83AE04" "x841807" "x85FD17" "x8653CD" "x88E18A" "x89B508" "x8A14DD" "x8A472B" "x8A81E9" "x8ADE60" "x8BC06E" "x8C8512" "x8CB305" "x8D15F4" "x8E7DDE" "x8F223E" "x9488F4" "x9B9815" "x9C2FE1" "xA004EE" "xA342A0" "xA4757E" "xA52195" "xA5D945" "xA61B4C" "xA8BE13" "xAB4E15" "xABC95C" "xAEFCFF" "xAFD467" "xB0A857" "xB44E90" "xB54252" "xB63189" "xB91A56" "xB96058" "xBA03F3" "xBD1FCD" "xBD522E" "xBEED31" "xC076A1" "xC190DC" "xC204D4" "xC332D6" "xC3FE77" "xC50C42" "xC604B8" "xC78B7B" "xC8E94D" "xC9CF83" "xC9FE01" "xCDF716" "xCE1919" "xCF1F63" "xD086FE" "xD46217" "xD6DC77" "xDBDA07" "xDD7015" "xDF8B34" "xDFD9BC" "xE06919" "xE07FAE" "xE540BF" "xE65F42" "xE720A3" "xE762BD" "xE7C123" "xE96600" "xEA9447" "xEB29C6" "xEB4BA3" "xEB9D15" "xED82AB" "xEEDF3F" "xF1469C" "xF7AB0A" "xF8035D" "xF850F7" "xF96DD0" "xFB6A23" "xFE2590" "xFFB04E" }
    traits = {}
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 28
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}
STATE_435 = {
    id = 435
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x0071EC" "x0A6FF4" "x0A71FC" "x10919C" "x12D818" "x14C515" "x166C7B" "x19D41F" "x1A5E4B" "x1BF1A3" "x1D5B7D" "x1F1DAF" "x20959A" "x243483" "x271E05" "x27F357" "x2B7B71" "x2E7766" "x2FBC96" "x30BDE4" "x33A1D9" "x371191" "x386E52" "x3886AF" "x3BF953" "x3D23A9" "x3D6B12" "x406633" "x437918" "x4383FB" "x48CDE8" "x4A0750" "x4CC41B" "x4E3616" "x5249AC" "x52DA83" "x53EF87" "x56929B" "x58C5C0" "x5BF5B5" "x5CD01C" "x645C02" "x64B36E" "x64EB7E" "x66AD05" "x66CE66" "x69346F" "x6ACC40" "x6C54D1" "x6CA5F2" "x6DF6DC" "x6E50D1" "x72DC2E" "x789091" "x79CBAB" "x7E18FF" "x813E64" "x81EB83" "x840CA3" "x844600" "x859D77" "x8A36B1" "x8C4727" "x8EDBBA" "x8F797F" "x8FCC7E" "x90C989" "x92695C" "x95354F" "x959C0D" "x9AF897" "x9D3C0F" "x9FBB27" "xA18C3D" "xA43BE4" "xA43CBF" "xA9CD4B" "xAF900D" "xAFA0D8" "xB1C9F7" "xB275F4" "xB3D80C" "xB50C4D" "xB894AD" "xB93351" "xBA1779" "xBCC90F" "xC02D27" "xC399C7" "xCA3EA3" "xCAA94E" "xCC526D" "xCD4666" "xCED9D2" "xD24DB6" "xD2C47B" "xD69B7E" "xD7C0E0" "xDE72D7" "xDEA1A2" "xE07A50" "xE177F7" "xE222D3" "xE67F2B" "xEAFDD4" "xEB52FC" "xEBAF36" "xECC372" "xED20A6" "xEE2416" "xEE425F" "xEEC079" "xF034D5" "xF08348" "xF153C1" "xF364AE" "xF37FBD" "xF4D788" "xF7E6BE" "xF9AC44" "xFA2B72" "xFB2520" "xFD930B" }
    traits = {}
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 10
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
    }
}
STATE_436 = {
    id = 436
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x008D0F" "x01322C" "x025B40" "x05A7F5" "x0A5428" "x0A8B45" "x0AC3D8" "x0BB603" "x0D93F9" "x0E0689" "x0F3D55" "x124283" "x153CDC" "x1A6457" "x214CA8" "x22E184" "x26E4D7" "x2F5B1F" "x3047F0" "x31C078" "x3305E7" "x364DB8" "x371111" "x373678" "x3CB5A4" "x40FA63" "x44482B" "x44FEA5" "x4576E8" "x4A9594" "x513F54" "x53C23E" "x55F0EC" "x5639D6" "x5866F5" "x5ABBBE" "x5F88A6" "x619931" "x61A78B" "x64200A" "x67CA7A" "x69372E" "x748E87" "x76AE42" "x792BFB" "x79983D" "x7AB0D1" "x7B6A96" "x7D375B" "x8317DB" "x845EB7" "x8815DA" "x8861D8" "x8A6780" "x8C1FF4" "x8CAD92" "x8DF7C1" "x8EAF5B" "x96A525" "x9E5BD8" "xA5AE21" "xA74D2E" "xA80C9F" "xAB80BE" "xB22330" "xB2ABD9" "xB2B281" "xB332DC" "xB52E11" "xBE3E06" "xC1A841" "xC1E277" "xD19947" "xD1C2FC" "xD74A11" "xD75720" "xD7C39D" "xDC17E4" "xDE013F" "xDE9677" "xE60365" "xE955A5" "xE99AEE" "xED21C9" "xF08224" "xF50DF3" "xF5923A" "xFD7C2B" "xFDBBB7" }
    traits = {}
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 68
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
    }
}
STATE_437 = {
    id = 437
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0012DF" "x025A10" "x029410" "x02F10F" "x03ACF0" "x03CB08" "x19627A" "x1ACEF7" "x1D9763" "x1F3773" "x207F1C" "x20A0D3" "x21805C" "x249618" "x2AA7C8" "x2EB43C" "x33EEDA" "x39203E" "x43EF83" "x4434A3" "x493060" "x4DA5F3" "x4FEEB4" "x50A8A3" "x52C186" "x5323DD" "x555EEA" "x56E3EE" "x5EAE8F" "x5EEEAB" "x63AF77" "x64F632" "x78B0BB" "x7BA08C" "x7C3AF6" "x7C8826" "x88E307" "x8D5B32" "x8EEA2C" "x998C50" "xA0A0BF" "xA33A34" "xA39A61" "xAAF911" "xAAFDF8" "xAC3287" "xACD768" "xB1C5AF" "xB66448" "xC20C7D" "xC39C70" "xCA075D" "xCA71A6" "xCC5762" "xD11F87" "xD3A91E" "xD4F36F" "xD8FC57" "xDA93A2" "xDE8ED9" "xE98C2F" "xF31D69" }
    traits = {}
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 104
    arable_resources = { bg_rye_farms bg_livestock_ranches }
    capped_resources = {
    }
}
STATE_438 = {
    id = 438
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0AF895" "x34870B" "x41FA43" "x49C056" "x4F4E17" "x5CA3CA" "x5CE45C" "x781DF0" "x8179BD" "x86FC9F" "x98D0E3" "x991FC5" "xB39BF9" "xB67790" "xBA6BDA" "xBB5AEA" "xC4356C" "xC4DC1E" "xC67E8D" "xCF1363" "xD3428A" "xDC080A" "xDF2274" "xF181A1" }
    traits = {}
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 89
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
        bg_whaling = 1
    }
    naval_exit_id = 3306 #Odheongu Sea
}
STATE_439 = {
    id = 439
    subsistence_building = "building_subsistence_farms"
    provinces = { "x012D3B" "x12CF8E" "x1E1AE9" "x28FCD7" "x89989C" "x9B0BB1" "xA21BC8" "xA24873" "xA2F3BA" "xA776DD" "xADE1FA" "xB1AF47" "xBFF781" "xCB2492" "xD70DD7" "xDD193A" "xE8B438" "xF194E6" "xFAF6FF" "xFF3ED6" }
    traits = {}
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 237
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
        bg_whaling = 1
    }
    naval_exit_id = 3306 #Odheongu Sea
}
STATE_440 = {
    id = 440
    subsistence_building = "building_subsistence_farms"
    provinces = { "x022DE0" "x10125E" "x1480B9" "x270C8A" "x27C5FD" "x296855" "x2DC013" "x40BEA1" "x4F3B2D" "x6A87F4" "x6A9C7B" "x7A384E" "x8192DA" "x84099E" "x87DAA1" "x88CDCC" "x8C1299" "x8FB1A9" "x9D61CB" "xA109D9" "xB0284F" "xC4CA7A" "xC91D66" "xD10D86" "xD21532" "xD70114" "xD8B8D1" "xEDAE0B" "xFBEE01" }
    traits = {}
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 132
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_silk_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
        bg_whaling = 1
    }
    naval_exit_id = 3306 #Odheongu Sea
}
STATE_441 = {
    id = 441
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x416119" "x4D3279" "x50BC2D" "x640179" "x90B940" }
    impassable = { "x640179" "x4D3279" }
    traits = {}
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 11
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
        bg_whaling = 1
    }
    naval_exit_id = 3307 #Widow's Sea
}